package dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


import model.User;

public interface UserJpaSpring extends JpaRepository<User, Integer> {
	
    User findByEmail(String email);
	
	
	

}
