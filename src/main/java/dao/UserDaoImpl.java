package dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import model.User;

@Repository
public class UserDaoImpl implements UsersDao {

	
	
	@Autowired
	UserJpaSpring jpaUser;
	
	@Override
	public void addUser(User user) {
		jpaUser.save(user);
		
	}

	@Override
	public List<User> returnsUsers() {
		// TODO Auto-generated method stub
		return jpaUser.findAll();
	}

	@Override
	public User returnUser(int id) {
		// TODO Auto-generated method stub
		return (User) jpaUser.findById(id).orElse(null);
	}

	@Override
	public void updateUser(User user) {
		// TODO Auto-generated method stub
		jpaUser.save(user);
	}

	@Override
	public void deleteUser(int id) {
		// TODO Auto-generated method stub
		jpaUser.deleteById(id);
	}

}
