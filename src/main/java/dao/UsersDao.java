package dao;

import java.util.List;

import model.User;

public interface UsersDao {
	
	//agrega usuario
    void addUser(User user);	
	//lista a todos los usuarios
	List<User> returnsUsers();
	//busca al usuario por id
	User returnUser(int id);
	//actualizar usuario 
	void updateUser(User user);
	//elimina usuario
	void deleteUser(int id);	
	

}
