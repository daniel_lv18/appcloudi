package utilidades;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ExtraerFecha {
	
	public static String FechaActual() {
		
		LocalDateTime now = LocalDateTime.now();

        

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        String formatDateTime = now.format(formatter);

        
        
		return formatDateTime;
	}
	
	

}
