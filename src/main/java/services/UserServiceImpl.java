package services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import dao.UsersDao;
import model.User;
import utilidades.ExtraerFecha;

@Service
public class UserServiceImpl implements UserService {

	
	@Autowired
	UsersDao dao;
	@Override
	public boolean agregarUsuario(User user) {
		try {
			
			dao.addUser(user);
			return true;
		
		}
		catch(Exception $e){
			System.out.println("algo fallo");
			return false;
		}
	}

	@Override
	public List<User> recuperarUsuarios() {
		// TODO Auto-generated method stub
		 return dao.returnsUsers();
	}

	@Override
	public void actualizarUsuario(int id,User user) {
			
		//aca es donde debo jugar con la logica
		//buscar ala base de datos por id
		//actualizar con lo que encontro en la bbdd
		if(dao.returnUser(id)!=null){
			dao.updateUser(user);			
		}
		
	}

	@Override
	public boolean eliminarUsuario(int id) {
		if(dao.returnUser(id)!=null) {
			dao.deleteUser(id);
			return true;
		}
		return false;
	}

	@Override
	public User buscarUsuario(int id) {
		// TODO Auto-generated method stub
		return dao.returnUser(id);
	}

}
