package services;

import java.util.List;

import model.User;

public interface UserService {
	
	boolean agregarUsuario(User user);
	
	List<User> recuperarUsuarios();
	
	void actualizarUsuario(int id,User user);
	
	boolean eliminarUsuario(int id);
	
	User buscarUsuario(int id);

}
