package controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import model.User;
import services.UserService;


@CrossOrigin(origins="*")
@RequestMapping(path="/users")
@RestController
public class UserController {
	
	@Autowired
	UserService service;
	

	@GetMapping(value="getusers", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<User> recuperarContactos(){
		return service.recuperarUsuarios();
	}
	
	@GetMapping(value="getusersById/{userId}", produces=MediaType.APPLICATION_JSON_VALUE)
	public User recuperarContactos(@PathVariable("userId") int id){
		return service.buscarUsuario(id);
	}
	
	@PostMapping(value="createUsers", consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
	public String guardarContacto(@RequestBody User usuario) {
		return String.valueOf(service.agregarUsuario(usuario));
	}
	
	@PutMapping(value="updateUsersById/{userId}", consumes=MediaType.APPLICATION_JSON_VALUE)
	public void actualizarContacto(@PathVariable("userId") int idusuario,@RequestBody User user) {
		service.actualizarUsuario(idusuario,user);
	}
	
	@DeleteMapping(value="deleteUsersById/{userId}")
	public void eliminarPorId(@PathVariable("userId") int id) {
		service.eliminarUsuario(id);
	}
	

}
